import { Component, OnInit, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { throttle, takeUntil } from 'rxjs/operators';
import { Subject, interval } from 'rxjs';

import { SocketService, UserService } from 'src/app/services';
import { UserLogin } from 'src/app/models';

@Component({
  selector: 'app-chatroom',
  templateUrl: './chatroom.component.html',
  styleUrls: ['./chatroom.component.scss']
})
export class ChatroomComponent implements OnInit {
  copied: boolean = false;
  roomId: number;
  user: UserLogin;
  typing: any = {};
  typingTimeout: any;
  joinedUsers: any[] = [];
  messages: any[] = [];
  message: string;
  
  private messageSubject: Subject<any> = new Subject();
  private userUnsubscribe$: Subject<any> = new Subject();
  private chatUnsubscribe$: Subject<any> = new Subject();

  constructor(private router: Router, 
              private userService: UserService,
              private route: ActivatedRoute,
              private socketService: SocketService) { }

  ngOnInit(): void {
    this.roomId = this.route.snapshot.params.id;

    this.userService.userInfo$
      .pipe(takeUntil(this.userUnsubscribe$))
      .subscribe(user => {
        this.user = user;
        if (user) {
          this.userUnsubscribe$.next();
          this.userUnsubscribe$.complete()
        }
      });

    this.messageSubject
      .pipe(
        takeUntil(this.chatUnsubscribe$),
        throttle(val => interval(900))
      ).subscribe(resp => {
        this.socketService.typing(resp);
      });

    this.socketService.receivedTyping()
      .pipe(takeUntil(this.chatUnsubscribe$))
      .subscribe(res => {
        if (res.roomId !== this.roomId) { return; }
        this.typing = { isTyping: true, ...res }
        if (this.typingTimeout) {
          clearTimeout(this.typingTimeout)
        }
        this.typingTimeout = setTimeout(() => {
          this.typing = { isTyping: false };
        }, 1000);
      });

    this.socketService.joinedRoomReceived()
      .pipe(takeUntil(this.chatUnsubscribe$))
      .subscribe(res => {     
        this.messages = res.messages;
      });
    
    this.socketService.newMessageReceived()
      .pipe(takeUntil(this.chatUnsubscribe$))
      .subscribe(res => {
        if (res.roomId !== this.roomId) { return; }
        this.messages.push(res);
      });

    this.socketService.receivedDestory()
      .pipe(takeUntil(this.chatUnsubscribe$))
      .subscribe(res => {
        if (res.roomId !== this.roomId) { return; }
        this.chatUnsubscribe$.next();
        this.chatUnsubscribe$.complete();
        this.router.navigate(['/']);
      });

    this.socketService.userList$.subscribe(res =>{
      this.joinedUsers = res;
    })
  }

  onSend(): void {
    this.socketService.sendMessage({ 
      userId: this.user.id, 
      roomId: this.roomId, 
      message: this.message 
    });
    this.message = '';
  }

  onWriteMessage(e: any): void {
    this.messageSubject.next({ userId: this.user.id, roomId: this.roomId });
  }

  onCopyChatRoom(): void {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = location.origin + '/login/' + this.roomId;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.copied = true;

    setTimeout(() => {
      this.copied = false;
    }, 2000)
  }

  onDestroyChatRoom(): void {
    this.socketService.destory({ roomId: this.roomId });
  }

  @HostListener('document:keydown', ['$event']) 
  onKeydownHandler(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.onSend();
    } 
  }

}
