import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup } from "@angular/forms";
import { UserService, SocketService } from '../../services/index';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  error: string = '';
  roomId: any;
  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private socketService: SocketService,
              private router: Router,
              private route: ActivatedRoute) { 
                this.createForm();
              }

  ngOnInit(): void {
    this.roomId = this.route.snapshot.params.id;

    this.socketService.createRoomReceived()
      .subscribe(res => {
        this.userService.setUserDate(res.user);
        this.router.navigate(['/chatroom', res.roomId]);
      }, err => {
        this.error = 'Something went wrong!';
      });

    this.socketService.joinedRoomReceived()
      .subscribe(res => {
        this.userService.setUserDate(res.user);
        this.router.navigate(['/chatroom', res.roomId]);
      }, err => {
        this.error = 'Something went wrong!';
      })
  }

  createForm(): void {
    this.form = this.formBuilder.group({
      username: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
    });
  }

  onSubmit(): void | string {
    if (this.form.invalid) {
      return this.error = 'Form is Invalid!';
    }
    if (this.roomId) {
      this.socketService.joinRoom({ roomId: this.roomId, user: { ...this.form.value } })
    } else {
      this.socketService.createRoom(this.form.value);
    }
  }

}
