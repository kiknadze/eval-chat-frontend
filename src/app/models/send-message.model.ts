export interface SendMessage {
    userId: number;
    roomId: number;
    message: string;
}