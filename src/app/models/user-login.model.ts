export interface UserLogin {
    email: string;
    username: string;
    id?: number;
}