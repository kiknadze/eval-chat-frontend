import { UserLogin } from './user-login.model';

export interface Message {
    user: UserLogin;
    roomId: number;
    message: string;
}