import { UserLogin } from './user-login.model';

export interface UserJoin {
    user: UserLogin;
    roomId: number;
}