import { UserLogin } from './user-login.model';
import { Message } from './message.model';

export interface UserJoined {
    user: UserLogin;
    roomId: number;
    messages: Message[];
}