import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserLogin } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _userInfo: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  userInfo$: Observable<any> = this._userInfo.asObservable();

  setUserDate(user: UserLogin): void {
      this._userInfo.next(user);
  }
  
}
