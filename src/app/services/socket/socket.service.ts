import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import * as io from 'socket.io-client';
import { environment } from '../../../environments/environment';

import { UserLogin, UserJoin, UserJoined, Message, Typing, Destroy, SendMessage } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private socket = io(environment.endpoint);

  userList: any[] = [];
  private _userList: BehaviorSubject<any> = new BehaviorSubject<any>(this.userList);
  userList$: Observable<any> = this._userList.asObservable();

  createRoom(data: UserLogin): void {
    this.socket.emit('create', { user: data });
  }

  createRoomReceived(): Observable<UserJoin> {
    const observable = new Observable<UserJoin>(observer => {
      this.socket.on('created', (data) => {
        this.userList.push(data.user);
        this._userList.next(this.userList);
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  joinRoom(data: UserJoin): void {
    this.socket.emit('join', data);
  }

  joinedRoomReceived():Observable<UserJoined> {
    const observable = new Observable<UserJoined>(observer => {
      this.socket.on('joined', (data) => {
        this.userList = [data.user, ...data.userList];
        this._userList.next(this.userList);
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  sendMessage(data: SendMessage): void {
    this.socket.emit('message', data);
  }

  newMessageReceived(): Observable<Message> {
    const observable = new Observable<Message>(observer => {
      this.socket.on('message', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  typing(data: Typing) {
    this.socket.emit('typing', data);
  }

  receivedTyping():Observable<UserJoin> {
    const observable = new Observable<UserJoin>(observer => {
      this.socket.on('typing', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  destory(data: Destroy): void {
    this.socket.emit('destroy', data);
  }

  receivedDestory(): Observable<Destroy> {
    const observable = new Observable<Destroy>(observer => {
      this.socket.on('destroyed', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

}